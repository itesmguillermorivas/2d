﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kenneth : MonoBehaviour {

    public GameObject hadouken;
    public Transform referencia;
    private Animator anim;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

        float h = Input.GetAxis("Horizontal");
        anim.SetFloat("velocidad", h);
        transform.Translate(h * Time.deltaTime * 5, 0, 0);

        if (Input.GetKeyUp(KeyCode.Space)) {

            // triggered
            anim.SetTrigger("hadouken");
        }
	}

    void OnCollisionEnter2D(Collision2D c) {
        print("COLISION!");
    }

    // notita: en c# los métodos se llaman con mayúscula
    public void Hadouken() {
        print("HADOUKEN!");
        Instantiate(
            hadouken, 
            referencia.position, 
            hadouken.transform.rotation
            );
    }

    public void SeMovio(float f) {
        print(f);
    }
}
